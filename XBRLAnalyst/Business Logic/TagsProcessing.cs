using LumenWorks.Framework.IO.Csv;
using Npgsql;
using System;
using System.Collections.Generic;
using System.IO;
using System.Reflection;
using System.Text.RegularExpressions;


namespace XBRLAnalyst
{
    public class BasicTag
    {
        public String tagName;
        public List<int> elementIDs = new List<int>();

        public BasicTag(String s)
        {
            tagName = s.Trim();
        }
    }

    public class SingleTag
    {
        public bool isVisible = false;
        public int level = 0;
        public int tid = -1;
        public String visualName = null;
        public List<BasicTag> tags = new List<BasicTag>();
        public String formula = null;
        public String noteText = null;
        public List<Object> formulaTagDependencies = null;
        public String taxonomyName = "US-GAAP";
        public int position = -1;

        public override String ToString()
        {
            return visualName;
        }

        internal void ReadAltTags(String s)
        {
            String[] arr = s.Split(';');
            foreach (String a in arr)
            {
                tags.Add(new BasicTag(a));
            }
        }

        internal void Read(CsvReader reader)
        {
            if (reader.FieldCount > 0 && reader[0] != "")
                tid = int.Parse(reader[0]);
            if (reader.FieldCount > 1 && reader[1] != "")
            {
                if (reader[1].Substring(0, 1) == ">")
                {
                    visualName = reader[1].Substring(1);
                    level = 0;
                }
                else
                {
                    visualName = reader[1];
                    level = 1;
                }
            }
            if (reader.FieldCount > 2 && reader[2] != "")
                tags.Add(new BasicTag(reader[2]));
            if (reader.FieldCount > 3 && reader[3] != "")
                ReadAltTags(reader[3]);
            if (reader.FieldCount > 4 && reader[4] != "")
                formula = reader[4];
        }
    }

    public class SpecialTagTracking    //Defines special required tag that have Text value and should always be queried from DB
    {
        public int tid = 0;
        public String visualName = null;
        public int inxTagsByTID = 0;
        public bool isSelected = false;
        public int inxSelected = 0;

        public SpecialTagTracking(int tid, String visualName, int inxTagsByTID)
        {
            this.tid = tid;
            this.visualName = visualName;
            this.inxTagsByTID = inxTagsByTID;
        }

        public void ClearSelected()
        {
            this.isSelected = false;
            this.inxSelected = 0;
        }

    }


    public class TagsProcessing
    {
        // List of all tags in our vocabulary
        public Dictionary<int, SingleTag> tagsByTID = new Dictionary<int, SingleTag>();
        public Dictionary<int, SingleTag> tagsByElementID = new Dictionary<int, SingleTag>();
        public List<SpecialTagTracking> lstSpecialTags = new List<SpecialTagTracking>();
        public List<SingleTag> tagsSpecial = new List<SingleTag>();
        public int TIDEndOfPeriod;

        public String ReadDataFileWithTagsDefinitions()
        {
            String error = "";
            CsvReader reader = null;
            try
            {
                String filename = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + @"\data_definitions.csv";                
                if (!File.Exists(filename))
                {
                    
                    Stream stream = Assembly.GetExecutingAssembly().GetManifestResourceStream("XBRLAnalyst.data_definitions.csv");
                    FileStream fs = new FileStream(filename, FileMode.Create);

                    byte[] buffer = new byte[8 * 1024];
                    int len;
                    while ((len = stream.Read(buffer, 0, buffer.Length)) > 0)
                    {
                        fs.Write(buffer, 0, len);
                    }
                    fs.Close();
                    fs.Dispose();
                }
                TextReader tr = new StreamReader(filename);
                reader = new CsvReader(tr, false, ',');
            }
            catch (Exception)
            {
                error = "\"data_definitions.csv\" file is probably open in another application.";
                return error;
            }                        
            SingleTag tag = null, tmpTag = null;

            while (reader.ReadNextRecord())
            {
                tag = new SingleTag();

                try
                {
                    tag.Read(reader);
                }
                catch (Exception e)
                {
                    error += "Exception parsing data occurred: " + e.Message;
                    continue;
                }


                // If we already have this tag ID, skip it
                if (tagsByTID.TryGetValue(tag.tid, out tmpTag))
                {
                    error += String.Format("Tag {0} has the same Tag ID {1} as another tag: {2}.\n", tag.visualName, tag.tid, tmpTag.visualName);
                    continue;
                }
                tagsByTID.Add(tag.tid, tag);
            }

            // Check for special required tags (Ticker and EndofPeriod) and store their TIDs in lstSpecialTags
            foreach (KeyValuePair<int, SingleTag> st in tagsByTID)
            {
                if (st.Value.visualName.Contains("Ticker"))
                    lstSpecialTags.Add(new SpecialTagTracking(st.Value.tid,"Ticker",st.Key));
                if (st.Value.visualName.Contains("End of Period"))
                {
                    lstSpecialTags.Add(new SpecialTagTracking(st.Value.tid, "End of Period", st.Key));
                    TIDEndOfPeriod = lstSpecialTags[lstSpecialTags.Count - 1].tid;
                }
                if (lstSpecialTags.Count==2)
                    break;
            }
            if (lstSpecialTags.Count != 2)
                error += "One of required tags (\"Ticker\", \"End of Period\") is missing in the data_definitions.csv file.";

            foreach (SpecialTagTracking spt in lstSpecialTags)
                tagsSpecial.Add(tagsByTID[spt.inxTagsByTID]);

            foreach (SingleTag st in tagsByTID.Values)
            {
                BuildVisualNotes(st);
            }

            foreach (SingleTag st in tagsByTID.Values)
            {
                RecursiveFormulaProcessing(st);
            }

            return error;
        }

        private void BuildVisualNotes(SingleTag st)
        {
            if (st.formula != null && st.formula != "")
            {
                st.noteText = st.formula;
                String name;
                SingleTag tmpTag;
                Regex rx = new Regex(@"\[([a-z0-9A-Z]+)\]");
                MatchCollection match = rx.Matches(st.formula);
                foreach (Match sm in match)
                {
                    name = sm.Groups[1].Value;
                    int tid = 0;
                    // Else, try to see if the name is numeric, to try to find a tag by TID
                    if (int.TryParse(name, out tid) && tagsByTID.TryGetValue(tid, out tmpTag))
                    {
                        st.noteText = st.noteText.Replace(name, tmpTag.visualName);
                    }
                }
            }
            else
                st.noteText = st.visualName;
        }

        private void RecursiveFormulaProcessing(SingleTag st)
        {
            if (st.formula != null && st.formula != "")
            {
                String name;
                SingleTag tmpTag;
                Regex rx = new Regex(@"\[([a-z0-9A-Z]+)\]");
                MatchCollection match = rx.Matches(st.formula);
                foreach (Match sm in match)
                {
                    name = sm.Groups[1].Value;
                    int tid = 0;
                    // Else, try to see if the name is numeric, to try to find a tag by TID
                    if (int.TryParse(name, out tid) && tagsByTID.TryGetValue(tid, out tmpTag) && tmpTag.formula != null && tmpTag.formula != "")
                    {
                        RecursiveFormulaProcessing(tmpTag);
                        st.formula = st.formula.Replace(String.Format("[{0}]", name), "(" + tmpTag.formula + ")");
                    }
                }
            }
        }

        internal void FindAndAddQname(String local_name, int element_id)
        {
            foreach (SingleTag st in tagsByTID.Values)
            {
                if (st.level == 0)
                    continue;
                foreach (BasicTag bt in st.tags)
                    if (bt.tagName == local_name)
                    {
                        try
                        {
                            bt.elementIDs.Add(element_id);
                            tagsByElementID.Add(element_id, st);
                        }
                        catch (Exception)    // Can be caused when the same tag appears in several places of data_definitions
                        {
                            // No behaviour is defined yet
                        }

                    }
            }
        }

        public String UpdateQnameIDFromDB()
        {
            String list = "", error="";
            NpgsqlDataReader reader;
            foreach (SingleTag st in tagsByTID.Values)
            {
                foreach (BasicTag bt in st.tags)
                    list += "'" + bt.tagName + "',";
            }
            char[] trimChars = { ',' };
            list = list.Trim(trimChars);
            list = list.Replace(",,", ",");
            try
            {
                String qry = String.Format("SELECT DISTINCT element_id, local_name FROM qname, element WHERE qname.qname_id = element.qname_id AND local_name IN ({0}) ORDER BY local_name", list);
                NpgsqlCommand command = new NpgsqlCommand(qry, BusinessLogic.businessLogic.dbConnection);
                reader = command.ExecuteReader();
            }
            catch (Exception e)
            {
                error += "Error querying element IDs: " + e.Message;
                return error;
            }

            int element_id = 0;
            String local_name = null;
            while (reader.Read())
            {
                try
                {
                    element_id = reader.GetInt32(0);
                }
                catch (Exception)
                {
                    element_id = -1;
                }
                try
                {
                    local_name = reader.GetString(1);
                }
                catch (Exception)
                {
                    local_name = null;
                }
                FindAndAddQname(local_name, element_id);
            }
            return error;
        }


        public void FillFormulaData()
        {
            foreach (SingleTag st in tagsByTID.Values)
            {
                // Check if there is a formula and in it any dependant tags
                if (st.tags.Count == 0 && st.formula != null && st.formula != "")
                {
                    try
                    {
                        String name;
                        Regex rx = new Regex(@"\[([a-z0-9A-Z]+)\]");
                        MatchCollection match = rx.Matches(st.formula);
                        foreach (Match sm in match)
                        {
                            name = sm.Groups[1].Value;
                            // Where should we add the tag to
                            SingleTag tmpTag = null;
                            if (st.formulaTagDependencies == null)
                                st.formulaTagDependencies = new List<object>();
                            // We have a few hardcoded parameter names coming from Finansu's Formulas
                            int tid = 0;
                            // Else, try to see if the name is numeric, to try to find a tag by TID
                            if (int.TryParse(name, out tid) && tagsByTID.TryGetValue(tid, out tmpTag))
                                st.formulaTagDependencies.Add(tmpTag);
                            // if we could not find it, last try is to go through the list of tags to see if it's there by visual name
                            foreach (SingleTag st1 in tagsByTID.Values)
                            {
                                foreach (BasicTag bt in st1.tags)
                                {
                                    // Did we find a tag?
                                    if (bt.tagName == name)
                                    {
                                        st.formulaTagDependencies.Add(st1);
                                        break;
                                    }
                                }
                            }
                        }
                    }
                    catch (Exception)
                    {
                    }
                }
            }
        }
    }
}