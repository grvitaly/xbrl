using Npgsql;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace XBRLAnalyst
{
    public class FiscalPeriod
    {
        public int Year;
        public string Period;
        public int cPosition;
        public FiscalPeriod(int Year, string Period)
        {
            this.Year = Year;
            this.Period = Period;
        }
    }
    public class SingleFact
    {
        public CompanyEntity company;
        public int year;
        public String period;
        public SingleTag tag;
        public String actualSelectedTag = null;
        public decimal factValue;
        public String strValue;
        public String strUnits;

        public SingleFact(CompanyEntity company, int year, String period, SingleTag fact, decimal factValue, String strValue="N/A", String strUnits="N/A")
        {
            this.company = company;
            this.period = period;
            this.year = year;
            this.tag = fact;
            this.factValue = factValue;
            this.strValue = strValue;
            this.strUnits = strUnits;
        }
        public string PeriodToString()
        {
            return this.year.ToString()+this.period;
        }
    }

    public class FactData
    {
        public List<SingleFact> allFacts = new List<SingleFact>();

        internal String JoinAllEntityIDs(IList companies)
        {
            String s = "";
            foreach (CompanyEntity ce in companies)
            {
                s += ce.entity_id + ",";
            }
            s = s.Trim(',');
            return s;
        }

        internal String JoinAllTagIDs(IList tags)
        {
            String s = "";
            foreach (SingleTag ce in tags)
            {
                try
                {
                    if (ce.level == 0)
                        continue;
                    ce.isVisible = true;
                    foreach (BasicTag bt in ce.tags)
                    {
                        if (bt.elementIDs != null && bt.elementIDs.Count > 0)
                            s += String.Join(",", bt.elementIDs) + ",";
                    }
                    if (ce.formulaTagDependencies != null)
                        foreach (object fd in ce.formulaTagDependencies)
                        {
                            if (fd.GetType().ToString() == "XBRLAnalyst.SingleTag")
                            {
                                foreach (BasicTag bt in ((SingleTag)fd).tags)
                                {
                                    if (bt.elementIDs != null && bt.elementIDs.Count > 0)
                                        s += String.Join(",", bt.elementIDs) + ",";
                                }
                            }
                        }
                }
                catch (Exception)
                { }
            }
            s = s.Trim(',');
            s = s.Replace(",,", ",");
            return s;
        }

        public void GetAllFacts(IList tags, IList companies, List<KeyValuePair<int, String>> timePeriods)
        {
            // Reset everything first
            allFacts.Clear();            

            String joinedTagIds = JoinAllTagIDs(tags);
            String joinedEntityIDs = JoinAllEntityIDs(companies);
            String joinedYears = "(";
            String qry = null;
            decimal effective_value;
            String fact_value;
            int entity_id, element_id, fiscal_year;
            String fiscal_period, ticker, strUnit;
            SingleFact fact = null;
            CompanyEntity company;
            SingleTag tag;
            Dictionary<int,String> Tickers=new Dictionary<int,string>();
            int inxFact;
            List<CompanyEntity> lstCompaniesRequery = new List<CompanyEntity>(); // List of companies whose End of Period Fact was found
            foreach (CompanyEntity ce in companies) // All by default
                lstCompaniesRequery.Add(ce);


            // Create joined string of special tags IDs and eliminate those from joinedTagIds
            String joinedSpecialTagIds = JoinAllTagIDs(BusinessLogic.businessLogic.tagsProcessor.tagsSpecial);
            joinedTagIds = "," + joinedTagIds + ",";
            foreach (String str in joinedSpecialTagIds.Split(','))
                joinedTagIds = joinedTagIds.Replace("," + str+",", ",");
            joinedTagIds = joinedTagIds.TrimStart(',');
            joinedTagIds = joinedTagIds.TrimEnd(',');
            if (joinedSpecialTagIds == "")
                joinedSpecialTagIds = "-1";
            if (joinedTagIds == "")
                joinedTagIds = "-1";

            String qrySpecial = null;
            String tickerParser="substring(entry_url from \'http://www.sec.gov/Archives/edgar/data/[0-9]+/[0-9]+/([a-z]+)-[0-9]+\\.xml\')";
            
            allFacts.Clear();
            NpgsqlDataReader reader;

            //First, query for tickers using entry_url since that is the key value required all the times that can be missing in Tag filing
            qry = String.Format("SELECT DISTINCT entity_id, " + tickerParser + " FROM accession WHERE entity_id IN ({0}) AND restatement_index = 1 AND period_index = 1",
                    joinedEntityIDs);
            try
            {
                NpgsqlCommand command = new NpgsqlCommand(qry, BusinessLogic.businessLogic.dbConnection);
                reader = command.ExecuteReader();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Fun: GetAllFacts; " + ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            while (reader.Read())
            {
                try
                {
                    entity_id = reader.GetInt32(0);
                }
                catch (Exception)
                {
                    entity_id = -1;
                }
                try
                {
                    ticker = reader.GetString(1);
                }
                catch (Exception)
                {
                    ticker = BusinessLogic.businessLogic.FactValueMissing;
                }
                if (BusinessLogic.businessLogic.dataMining.allCompanies.TryGetValue(entity_id, out company))
                {
                    Tickers.Add(company.entity_id,ticker.ToLower());
                }
            }
            reader.Close();


            if (timePeriods == null)
            {
                qry = String.Format("SELECT entity_id, element_id, effective_value, fiscal_year, fiscal_period, uom FROM fact, accession, context WHERE context.context_id = fact.context_id AND accession.accession_id = fact.accession_id AND element_id IN ({0}) AND entity_id IN ({1}) AND specifies_dimensions = 'f' AND restatement_index = 1 AND ultimus_index = 1 AND fiscal_period in ('1Q', '2Q', '3Q', '4Q', 'Y')",
                        joinedTagIds, joinedEntityIDs);
                qrySpecial = String.Format("SELECT entity_id, element_id, fact_value, fiscal_year, fiscal_period FROM fact, accession, context WHERE context.context_id = fact.context_id AND accession.accession_id = fact.accession_id AND element_id IN ({0}) AND entity_id IN ({1}) AND specifies_dimensions = 'f' AND restatement_index = 1 AND ultimus_index = 1 AND fiscal_period in ('1Q', '2Q', '3Q', '4Q', 'Y')",
                        joinedSpecialTagIds, joinedEntityIDs);
            }
            else
            {
                foreach (KeyValuePair<int, String> pair in timePeriods)
                {
                    String period;
                    if (pair.Value == "Annual")
                        period = "Y";
                    else
                        period = pair.Value;
                    joinedYears += String.Format("(fiscal_year = {0} AND fiscal_period = '{1}') OR ", pair.Key, period);
                }
                joinedYears = joinedYears.Substring(0, joinedYears.Length - 4);
                joinedYears += ")";

                qry = String.Format("SELECT entity_id, element_id, effective_value, fiscal_year, fiscal_period, uom FROM fact, accession, context WHERE context.context_id = fact.context_id AND accession.accession_id = fact.accession_id AND {0} AND element_id IN ({1}) AND entity_id IN ({2}) AND specifies_dimensions = 'f' AND restatement_index = 1 AND ultimus_index = 1",
                    joinedYears, joinedTagIds, joinedEntityIDs);
                qrySpecial = String.Format("SELECT entity_id, element_id, fact_value, fiscal_year, fiscal_period FROM fact, accession, context WHERE context.context_id = fact.context_id AND accession.accession_id = fact.accession_id AND {0} AND element_id IN ({1}) AND entity_id IN ({2}) AND specifies_dimensions = 'f' AND restatement_index = 1 AND ultimus_index = 1",
                    joinedYears, joinedSpecialTagIds, joinedEntityIDs);
            }
            try
            {
                NpgsqlCommand command = new NpgsqlCommand(qry, BusinessLogic.businessLogic.dbConnection);
                reader = command.ExecuteReader();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }


            while (reader.Read())
            {
                try
                {
                    entity_id = reader.GetInt32(0);
                }
                catch (Exception)
                {
                    entity_id = int.MinValue;
                }
                try
                {
                    element_id = reader.GetInt32(1);
                }
                catch (Exception)
                {
                    element_id = int.MinValue;
                }
                try
                {
                    effective_value = reader.GetDecimal(2);
                    fact_value = "";
                }
                catch (Exception)
                {
                    effective_value = decimal.MinValue;
                    fact_value = BusinessLogic.businessLogic.FactValueMissing;
                }
                try
                {
                    fiscal_year = reader.GetInt32(3);
                }
                catch (Exception)
                {
                    fiscal_year = int.MinValue;
                }
                try
                {
                    fiscal_period = reader.GetString(4);
                }
                catch (Exception)
                {
                    fiscal_period = null;
                }

                try
                {
                    strUnit = reader.GetString(5);
                }
                catch (Exception)
                {
                    strUnit = BusinessLogic.businessLogic.FactValueMissing;
                }

                if (BusinessLogic.businessLogic.dataMining.allCompanies.TryGetValue(entity_id, out company))
                {
                    if (BusinessLogic.businessLogic.tagsProcessor.tagsByElementID.TryGetValue(element_id, out tag))
                    {
                        fact = new SingleFact(company, fiscal_year, fiscal_period, tag, effective_value, fact_value.ToLower(), strUnit);
                        foreach (BasicTag bt in tag.tags)
                            foreach (int tagID in bt.elementIDs)
                                if (tagID == element_id)
                                {
                                    fact.actualSelectedTag = bt.tagName;
                                    break;
                                }
                        allFacts.Add(fact);
                    }
                }
            }
            reader.Close();

            // Add special Tags with text values
            try
            {
                NpgsqlCommand command = new NpgsqlCommand(qrySpecial, BusinessLogic.businessLogic.dbConnection);
                reader = command.ExecuteReader();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            while (reader.Read())
            {
                try
                {
                    entity_id = reader.GetInt32(0);
                }
                catch (Exception)
                {
                    entity_id = int.MinValue;
                }
                try
                {
                    element_id = reader.GetInt32(1);
                }
                catch (Exception)
                {
                    element_id = int.MinValue;
                }
                try
                {
                    fact_value = reader.GetString(2);
                }
                catch (Exception)
                {
                    fact_value = BusinessLogic.businessLogic.FactValueMissing;
                }
                try
                {
                    fiscal_year = reader.GetInt32(3);
                }
                catch (Exception)
                {
                    fiscal_year = int.MinValue;
                }
                try
                {
                    fiscal_period = reader.GetString(4);
                }
                catch (Exception)
                {
                    fiscal_period = null;
                }

                if (BusinessLogic.businessLogic.dataMining.allCompanies.TryGetValue(entity_id, out company))
                {
                    if (BusinessLogic.businessLogic.tagsProcessor.tagsByElementID.TryGetValue(element_id, out tag))
                    {
                        fact = new SingleFact(company, fiscal_year, fiscal_period, tag, -1, fact_value);
                        foreach (BasicTag bt in tag.tags)
                            foreach (int tagID in bt.elementIDs)
                                if (tagID == element_id)
                                {
                                    fact.actualSelectedTag = bt.tagName;
                                    break;
                                }
                        if (fact.tag.tid == BusinessLogic.businessLogic.tagsProcessor.TIDEndOfPeriod && 
                            fact.strValue != BusinessLogic.businessLogic.FactValueMissing && fact.strValue != "")
                            lstCompaniesRequery.Remove(company); 
                        allFacts.Add(fact);
                    }
                }
            }
            reader.Close();

            // Requery the DB for companies whose "End of Period" fact was missing
            if (lstCompaniesRequery.Count != 0)
            {
                qrySpecial = String.Format("SELECT DISTINCT entity_id, period_end, fiscal_year, fiscal_period FROM fact, accession, context WHERE context.context_id = fact.context_id  AND accession.accession_id = fact.accession_id AND {0} AND entity_id IN ({1}) AND specifies_dimensions = 'f' AND restatement_index = 1 AND ultimus_index = 1",
                    joinedYears, JoinAllEntityIDs(lstCompaniesRequery));
                try
                {
                    NpgsqlCommand command = new NpgsqlCommand(qrySpecial, BusinessLogic.businessLogic.dbConnection);
                    reader = command.ExecuteReader();
                    BusinessLogic.businessLogic.tagsProcessor.tagsByTID.TryGetValue(BusinessLogic.businessLogic.tagsProcessor.TIDEndOfPeriod, out tag);
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }
                while (reader.Read())
                {
                    try
                    {
                        entity_id = reader.GetInt32(0);
                    }
                    catch (Exception)
                    {
                        entity_id = int.MinValue;
                    }
                    try
                    {
                        fact_value = reader.GetDate(1).AddDays(-1).ToString(); // Shift one day back due to filings particularities
                        if (fact_value == "")
                            continue;
                    }
                    catch (Exception)
                    {
                        fact_value = BusinessLogic.businessLogic.FactValueMissing;
                    }
                    try
                    {
                        fiscal_year = reader.GetInt32(2);
                    }
                    catch (Exception)
                    {
                        fiscal_year = int.MinValue;
                    }
                    try
                    {
                        fiscal_period = reader.GetString(3);
                    }
                    catch (Exception)
                    {
                        fiscal_period = null;
                    }

                    if (BusinessLogic.businessLogic.dataMining.allCompanies.TryGetValue(entity_id, out company))
                    {
                        inxFact = allFacts.FindIndex(x => (x.company == company && x.year == fiscal_year && x.period == fiscal_period && x.tag == tag));
                        if (inxFact == -1)
                        {
                            fact = new SingleFact(company, fiscal_year, fiscal_period, tag, -1, fact_value);
                            allFacts.Add(fact);
                        }
                        else
                            allFacts[inxFact].strValue = fact_value;
                    }
                }
                reader.Close();
            }


            bool found = false;
            String strValue = "";
            List<SingleFact> difFact= new List<SingleFact>();

            try
            {
                if (timePeriods == null) // Historical form
                {
                    difFact = allFacts.GroupBy(x => x.PeriodToString()).Select(x => x.First()).ToList();
                }
                foreach (CompanyEntity ce in companies)
                {
                    foreach (SingleTag st in tags)
                    {
                        found = false;
                        foreach (SingleFact sf in allFacts)
                        {
                            if (sf.tag == st && sf.company == ce)
                            {
                                found = true;
                                if (st.visualName.Contains("Ticker") & sf.strValue == "")
                                    sf.strValue = Tickers[ce.entity_id];
                                break;
                            }
                        }

                        if (st.visualName.Contains("Ticker")) //For Ticker tag, add the value from parser if the tag was not found
                            strValue = Tickers[ce.entity_id];
                        else
                            strValue = BusinessLogic.businessLogic.FactValueMissing;

                        if (!found)
                            if (timePeriods != null) //Comps table form
                            {
                                foreach (KeyValuePair<int, String> y in timePeriods)
                                {
                                    String period;
                                    if (y.Value == "Annual")
                                        period = "Y";
                                    else
                                        period = y.Value;
                                    fact = new SingleFact(ce, y.Key, period, st, decimal.MinValue, strValue);
                                    allFacts.Add(fact);
                                }
                            }
                            else //Historical data
                            {
                                foreach (SingleFact y in difFact)
                                {
                                    fact = new SingleFact(ce, y.year, y.period, st, decimal.MinValue, strValue);
                                    allFacts.Add(fact);
                                }
                            }
                        }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
        }
    }
}
