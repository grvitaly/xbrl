﻿using System;
using System.Text.RegularExpressions;
using ExcelDna.Integration;
using System.Windows.Forms;
using Npgsql;
using System.Diagnostics;


namespace XBRLAnalyst
{
    public static class XBRLfun
    {
        #region XBRLFact
        /// <summary>
        /// Returns the financial fact from XBRL filing of a public company.
        /// </summary>
        /// <param name="CIK">Company Index Key (CIK)</param>
        /// <param name="Tag">XBRL tag, e.g. SalesRevenueNet or NetIncomeLoss</param>
        /// <param name="Year">Fiscal year of the filing</param>
        /// <param name="Period">Fiscal period, e.g. "Y" for yearly or "Q1" for first quarter</param>
        /// <returns>Returns the value or NoData if no fact was found</returns>
        [ExcelFunction("Returns the financial fact from XBRL filing of a public company.")]
        public static object XBRLFact(
            [ExcelArgument("is the Company Index Key (CIK).", Name = "CIK")] string CIK,
            [ExcelArgument("is the XBRL tag, e.g. SalesRevenueNet or NetIncomeLoss.",
                Name = "Tag")] string Tag,
            [ExcelArgument("is the Fiscal year of the filing.", Name = "Year")] string Year,
            [ExcelArgument("is the Fiscal period, e.g. \"Y\" for yearly or \"2Q\" for second quarter.",
                Name = "Period")] string Period)
        {
            int entity_id=0;
            int element_id;
            string qry, fact_value = "nodata", listEntity_id = "";
            decimal effective_value;

            if (CIK == null || CIK == "" || Tag == null || Tag == "" || Year == null || Year == "" || Period == null || Period == "")
                return "NaN";

            if (BusinessLogic.businessLogic.dbConnection == null ||
                BusinessLogic.businessLogic.dbConnection.State != System.Data.ConnectionState.Open)
                return "NaN";

            try
            {
                //Find company's entity_id by CIK
                foreach (CompanyEntity ce in BusinessLogic.businessLogic.dataMining.allCompanies.Values)
                {
                    if (ce.entity_code==CIK)
                    {
                        entity_id=ce.entity_id;
                        break;
                    }

                }

                // Find element_id
                if (BusinessLogic.businessLogic.dbConnection == null)
                {
                    NativeWindow parent = new NativeWindow();
                    parent.AssignHandle(Process.GetCurrentProcess().MainWindowHandle);
                    if (MessageBox.Show(parent, "You do not have connection to database yet, would you like to connect to database now?", "Error", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                        new ConnectionDialog().ShowDialog(parent);
                    parent.ReleaseHandle();
                }
                qry = String.Format("SELECT element_id FROM element, qname WHERE qname.qname_id = element.qname_id AND qname.local_name='"+Tag+"'");
                NpgsqlCommand command = new NpgsqlCommand(qry, BusinessLogic.businessLogic.dbConnection);
                NpgsqlDataReader reader = command.ExecuteReader();
                while (reader.Read())
                {
                    try
                    {
                        element_id = reader.GetInt32(0);
                        listEntity_id = listEntity_id + element_id.ToString() + ",";
                    }
                    catch (Exception)
                    {
                    }
                }
                reader.Close();

                if (listEntity_id.Length>1)
                {
                    listEntity_id=listEntity_id.TrimEnd(',');
                    // Find the financial fact
                    qry = String.Format("SELECT fact_value, effective_value FROM fact, accession, context WHERE context.context_id = fact.context_id AND accession.accession_id = fact.accession_id AND entity_id={0} AND element_id in ({1}) AND fiscal_year = {2} AND fiscal_period = '{3}' AND specifies_dimensions = 'f' AND restatement_index = 1 AND ultimus_index = 1",
                            entity_id, listEntity_id, Year, Period);
                    command = new NpgsqlCommand(qry, BusinessLogic.businessLogic.dbConnection);
                    reader = command.ExecuteReader();
                    if (reader.Read())
                    {
                        try
                        {
                            fact_value = reader.GetString(0);
                        }
                        catch (Exception)
                        { }

                        try
                        {
                            effective_value = reader.GetDecimal(1);
                            reader.Close();
                            return effective_value;
                        }
                        catch (Exception)
                        { }
                    }
                    reader.Close();
                }

                return fact_value;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return ("NaN");
            }

        }


        #endregion
    }
}