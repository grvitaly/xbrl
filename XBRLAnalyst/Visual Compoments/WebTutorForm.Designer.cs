﻿namespace XBRLAnalyst
{
    partial class WebTutorForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.ProductNameLabel = new System.Windows.Forms.Label();
            this.CloseButton = new System.Windows.Forms.Button();
            this.ExcelDnaLicenseLabel = new System.Windows.Forms.Label();
            this.ExcelDnaLicenseLinkLabel = new System.Windows.Forms.LinkLabel();
            this.label1 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // ProductNameLabel
            // 
            this.ProductNameLabel.AutoSize = true;
            this.ProductNameLabel.Font = new System.Drawing.Font("Segoe UI", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ProductNameLabel.Location = new System.Drawing.Point(15, 12);
            this.ProductNameLabel.Margin = new System.Windows.Forms.Padding(3, 0, 0, 0);
            this.ProductNameLabel.Name = "ProductNameLabel";
            this.ProductNameLabel.Size = new System.Drawing.Size(159, 32);
            this.ProductNameLabel.TabIndex = 1;
            this.ProductNameLabel.Text = "XBRLAnalyst";
            // 
            // CloseButton
            // 
            this.CloseButton.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.CloseButton.Location = new System.Drawing.Point(69, 112);
            this.CloseButton.Name = "CloseButton";
            this.CloseButton.Size = new System.Drawing.Size(75, 23);
            this.CloseButton.TabIndex = 15;
            this.CloseButton.Text = "Close";
            this.CloseButton.UseVisualStyleBackColor = true;
            this.CloseButton.Click += new System.EventHandler(this.CloseButton_Click);
            // 
            // ExcelDnaLicenseLabel
            // 
            this.ExcelDnaLicenseLabel.AutoSize = true;
            this.ExcelDnaLicenseLabel.Location = new System.Drawing.Point(21, 58);
            this.ExcelDnaLicenseLabel.Margin = new System.Windows.Forms.Padding(3, 0, 3, 3);
            this.ExcelDnaLicenseLabel.Name = "ExcelDnaLicenseLabel";
            this.ExcelDnaLicenseLabel.Size = new System.Drawing.Size(169, 13);
            this.ExcelDnaLicenseLabel.TabIndex = 16;
            this.ExcelDnaLicenseLabel.Text = "To get help using our software,";
            // 
            // ExcelDnaLicenseLinkLabel
            // 
            this.ExcelDnaLicenseLinkLabel.ActiveLinkColor = System.Drawing.Color.Blue;
            this.ExcelDnaLicenseLinkLabel.AutoSize = true;
            this.ExcelDnaLicenseLinkLabel.LinkBehavior = System.Windows.Forms.LinkBehavior.HoverUnderline;
            this.ExcelDnaLicenseLinkLabel.Location = new System.Drawing.Point(21, 90);
            this.ExcelDnaLicenseLinkLabel.Margin = new System.Windows.Forms.Padding(3, 0, 3, 6);
            this.ExcelDnaLicenseLinkLabel.Name = "ExcelDnaLicenseLinkLabel";
            this.ExcelDnaLicenseLinkLabel.Size = new System.Drawing.Size(172, 13);
            this.ExcelDnaLicenseLinkLabel.TabIndex = 17;
            this.ExcelDnaLicenseLinkLabel.TabStop = true;
            this.ExcelDnaLicenseLinkLabel.Text = "http://findynamics.com/tutorials";
            this.ExcelDnaLicenseLinkLabel.VisitedLinkColor = System.Drawing.Color.Blue;
            this.ExcelDnaLicenseLinkLabel.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.ExcelDnaLicenseLinkLabel_LinkClicked);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(21, 74);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(138, 13);
            this.label1.TabIndex = 18;
            this.label1.Text = "please visit tutorials page";
            // 
            // WebTutorForm
            // 
            this.AcceptButton = this.CloseButton;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.CloseButton;
            this.ClientSize = new System.Drawing.Size(213, 146);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.ExcelDnaLicenseLabel);
            this.Controls.Add(this.ExcelDnaLicenseLinkLabel);
            this.Controls.Add(this.CloseButton);
            this.Controls.Add(this.ProductNameLabel);
            this.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "WebTutorForm";
            this.ShowInTaskbar = false;
            this.Text = "XBRLAnalyst tutorials";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label ProductNameLabel;
        private System.Windows.Forms.Button CloseButton;
        private System.Windows.Forms.Label ExcelDnaLicenseLabel;
        private System.Windows.Forms.LinkLabel ExcelDnaLicenseLinkLabel;
        private System.Windows.Forms.Label label1;
    }
}