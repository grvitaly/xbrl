using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading;
using System.Windows.Forms;

namespace XBRLAnalyst
{
    public partial class ConnectionDialog : Form
    {
        public ConnectionDialog()
        {
            InitializeComponent();
        }

        private void HandleAdjustProgress(int progress)
        {
            progressBar.Value = progress;
        }


        private void HandleThreadDone(String error)
        {
            if (error != null && error != "")
                MessageBox.Show(this, "Following errors occurred: " + error, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            DialogResult = DialogResult.OK;
        }

        private static void SetRequestData(HttpWebRequest request, string data)
        {
            byte[] streamData = Encoding.ASCII.GetBytes(data);
            request.ContentLength = streamData.Length;

            Stream dataStream = request.GetRequestStream();
            dataStream.Write(streamData, 0, streamData.Length);
            dataStream.Close();
        }

        private void ConnectButton_Click(object sender, EventArgs e)
        {
            this.Enabled = false;
            try
            {
                string url = String.Format("http://findynamics.com/check_registration?un={0}&pwd={1}", System.Uri.EscapeDataString(UserNameEB.Text), System.Uri.EscapeDataString(PasswordEB.Text));
                HttpWebRequest request = (HttpWebRequest)WebRequest.Create(url);
                CookieContainer cookies = new CookieContainer();

                request.CookieContainer = cookies;
                request.UserAgent = "Mozilla/5.0 (Windows; U; Windows NT 6.0; uk; rv:1.9.1.2) Gecko/20090729 Firefox/3.5.2 (.NET CLR 3.5.30729)";
                request.KeepAlive = true;
                request.Timeout = 120000;
                request.Method = "GET";
                request.ContentType = "application/x-www-form-urlencoded";
            
                HttpWebResponse response = (HttpWebResponse)request.GetResponse();
                string responseString = new StreamReader(response.GetResponseStream()).ReadToEnd();
                responseString = System.Uri.UnescapeDataString(responseString);

                if (responseString == null || responseString == "" || responseString == "-1")
                {
                    MessageBox.Show(this, "Cannot connect to website or your username / password is incorrect", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    this.Enabled = true;
                    return;
                }

                String[] params_arr = responseString.Split('|');

                String error = BusinessLogic.businessLogic.ConnectToDb(params_arr[0], params_arr[1], params_arr[2], params_arr[3], params_arr[4]);
                if (error == null || error == "")
                {
                    //ConnectButton.Enabled = false;
                    //SaveConnectionCB.Enabled = false;

                    ThreadWorker worker = new ThreadWorker();
                    worker.form = this;
                    worker.threadDone += HandleThreadDone;
                    worker.setProgress += HandleAdjustProgress;
                    new Thread(worker.RunInitialization).Start();
                }
                else
                {
                    MessageBox.Show(this, "Cannot connect to database: " + error, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    this.Enabled = true;
                }
            }
            catch (Exception)
            {
                this.Enabled = true;
                return;
            }
        }

        private void ConnectionDialog_Shown(object sender, EventArgs e)
        {
            try
            {
                RegistryKey key = Registry.CurrentUser.CreateSubKey(@"SOFTWARE\XBRLAnalyst", RegistryKeyPermissionCheck.ReadSubTree);
                UserNameEB.Text = (String)key.GetValue("Username", "your username");
                PasswordEB.Text = (String)key.GetValue("Password", "your password");
                if ((String)key.GetValue("SaveSettings", "True") == "True")
                    SaveConnectionCB.Checked = true;
                else
                    SaveConnectionCB.Checked = false;
            }
            catch (Exception)
            {
            }
        }

        private void ConnectionDialog_FormClosed(object sender, FormClosedEventArgs e)
        {
            if (SaveConnectionCB.Checked)
            {
                try
                {
                    RegistryKey key = Registry.CurrentUser.CreateSubKey(@"SOFTWARE\XBRLAnalyst", RegistryKeyPermissionCheck.ReadWriteSubTree);
                    key.SetValue("Username", UserNameEB.Text);
                    key.SetValue("Password", PasswordEB.Text);
                    if (SaveConnectionCB.Checked)
                        key.SetValue("SaveSettings", "True");
                    else
                        key.SetValue("SaveSettings", "False");
                }
                catch (Exception)
                {
                }
            }
        }

        private void SignUp_Click(object sender, EventArgs e)
        {
            System.Diagnostics.Process.Start("http://findynamics.com/login?action=register");
        }
    }

    public class ThreadWorker
    {
        public delegate void ThreadDone(String error);
        public delegate void SetProgress(int value);
        public event ThreadDone threadDone;
        public event SetProgress setProgress;
        public ConnectionDialog form;

        public void RaiseSetProgress(int progress)
        {
            if (setProgress != null)
                form.Invoke(setProgress, progress);
        }

        public void RaiseThreadDone(String error)
        {
            if (threadDone != null)
                form.Invoke(threadDone, error);
        }

        public void RunInitialization()
        {
            RaiseSetProgress(25);
            String error = BusinessLogic.businessLogic.tagsProcessor.ReadDataFileWithTagsDefinitions();
            if (error != "")
            {
                RaiseThreadDone(error);
                return;
            }


            RaiseSetProgress(30);
            error = BusinessLogic.businessLogic.tagsProcessor.UpdateQnameIDFromDB();
            if (error != "")
            {
                RaiseThreadDone(error);
                return;
            }
            RaiseSetProgress(50);
            BusinessLogic.businessLogic.tagsProcessor.FillFormulaData();
            RaiseSetProgress(75);
            BusinessLogic.businessLogic.dataMining.FillUpEntityLists();
            RaiseSetProgress(100);

            RaiseThreadDone(error);
        }
    }
}
