﻿namespace XBRLAnalyst
{
    partial class Sc2_Historical
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.CancelButtonB = new System.Windows.Forms.Button();
            this.FinishB = new System.Windows.Forms.Button();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.LoadSelectedCompanies = new System.Windows.Forms.Button();
            this.SelectCompaniesRefedit = new VS.NET_RefeditControl.refedit();
            this.label4 = new System.Windows.Forms.Label();
            this.MoveCompanyLeft = new System.Windows.Forms.Button();
            this.MoveCompanyRight = new System.Windows.Forms.Button();
            this.SelectedCompaniesLB = new System.Windows.Forms.ListBox();
            this.label1 = new System.Windows.Forms.Label();
            this.ListOfCompaniesLB = new System.Windows.Forms.ListBox();
            this.CompanyNameTB = new System.Windows.Forms.TextBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.LoadTags = new System.Windows.Forms.Button();
            this.SelectTagsRefedit = new VS.NET_RefeditControl.refedit();
            this.SelectedTags = new System.Windows.Forms.ListBox();
            this.MoveTagToLeft = new System.Windows.Forms.Button();
            this.MoveTagToRight = new System.Windows.Forms.Button();
            this.TagsTree = new System.Windows.Forms.TreeView();
            this.groupBox4.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // CancelButtonB
            // 
            this.CancelButtonB.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.CancelButtonB.Location = new System.Drawing.Point(312, 639);
            this.CancelButtonB.Name = "CancelButtonB";
            this.CancelButtonB.Size = new System.Drawing.Size(75, 23);
            this.CancelButtonB.TabIndex = 2;
            this.CancelButtonB.Text = "&Cancel";
            this.CancelButtonB.UseVisualStyleBackColor = true;
            this.CancelButtonB.Click += new System.EventHandler(this.CancelButtonB_Click_1);
            // 
            // FinishB
            // 
            this.FinishB.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.FinishB.Location = new System.Drawing.Point(412, 639);
            this.FinishB.Name = "FinishB";
            this.FinishB.Size = new System.Drawing.Size(75, 23);
            this.FinishB.TabIndex = 1;
            this.FinishB.Text = "&Finish";
            this.FinishB.UseVisualStyleBackColor = true;
            this.FinishB.Click += new System.EventHandler(this.FinishB_Click);
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.LoadSelectedCompanies);
            this.groupBox4.Controls.Add(this.SelectCompaniesRefedit);
            this.groupBox4.Controls.Add(this.label4);
            this.groupBox4.Controls.Add(this.MoveCompanyLeft);
            this.groupBox4.Controls.Add(this.MoveCompanyRight);
            this.groupBox4.Controls.Add(this.SelectedCompaniesLB);
            this.groupBox4.Controls.Add(this.label1);
            this.groupBox4.Controls.Add(this.ListOfCompaniesLB);
            this.groupBox4.Controls.Add(this.CompanyNameTB);
            this.groupBox4.Location = new System.Drawing.Point(12, 9);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(760, 230);
            this.groupBox4.TabIndex = 11;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "Select Company using CIK or Company Name (Only one company can be selected):";
            // 
            // LoadSelectedCompanies
            // 
            this.LoadSelectedCompanies.Location = new System.Drawing.Point(671, 39);
            this.LoadSelectedCompanies.Name = "LoadSelectedCompanies";
            this.LoadSelectedCompanies.Size = new System.Drawing.Size(75, 20);
            this.LoadSelectedCompanies.TabIndex = 13;
            this.LoadSelectedCompanies.Text = "Load";
            this.LoadSelectedCompanies.UseVisualStyleBackColor = true;
            this.LoadSelectedCompanies.Click += new System.EventHandler(this.LoadSelectedCompanies_Click);
            // 
            // SelectCompaniesRefedit
            // 
            this.SelectCompaniesRefedit._Excel = null;
            this.SelectCompaniesRefedit.AllowCollapsedFormResize = false;
            this.SelectCompaniesRefedit.AllowTextEntry = false;
            this.SelectCompaniesRefedit.AutoSize = true;
            this.SelectCompaniesRefedit.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.SelectCompaniesRefedit.Location = new System.Drawing.Point(416, 39);
            this.SelectCompaniesRefedit.Name = "SelectCompaniesRefedit";
            this.SelectCompaniesRefedit.Size = new System.Drawing.Size(249, 20);
            this.SelectCompaniesRefedit.TabIndex = 12;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.ForeColor = System.Drawing.Color.Green;
            this.label4.Location = new System.Drawing.Point(413, 20);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(186, 13);
            this.label4.TabIndex = 11;
            this.label4.Text = "Select Range of Cells containing CIKs";
            // 
            // MoveCompanyLeft
            // 
            this.MoveCompanyLeft.Location = new System.Drawing.Point(358, 135);
            this.MoveCompanyLeft.Name = "MoveCompanyLeft";
            this.MoveCompanyLeft.Size = new System.Drawing.Size(52, 23);
            this.MoveCompanyLeft.TabIndex = 9;
            this.MoveCompanyLeft.Text = "<<";
            this.MoveCompanyLeft.UseVisualStyleBackColor = true;
            this.MoveCompanyLeft.Click += new System.EventHandler(this.MoveCompanyLeft_Click);
            // 
            // MoveCompanyRight
            // 
            this.MoveCompanyRight.Location = new System.Drawing.Point(358, 106);
            this.MoveCompanyRight.Name = "MoveCompanyRight";
            this.MoveCompanyRight.Size = new System.Drawing.Size(52, 23);
            this.MoveCompanyRight.TabIndex = 9;
            this.MoveCompanyRight.Text = ">>";
            this.MoveCompanyRight.UseVisualStyleBackColor = true;
            this.MoveCompanyRight.Click += new System.EventHandler(this.MoveCompanyRight_Click);
            // 
            // SelectedCompaniesLB
            // 
            this.SelectedCompaniesLB.FormattingEnabled = true;
            this.SelectedCompaniesLB.Location = new System.Drawing.Point(416, 63);
            this.SelectedCompaniesLB.Name = "SelectedCompaniesLB";
            this.SelectedCompaniesLB.Size = new System.Drawing.Size(330, 160);
            this.SelectedCompaniesLB.TabIndex = 8;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.ForeColor = System.Drawing.Color.Green;
            this.label1.Location = new System.Drawing.Point(7, 20);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(323, 13);
            this.label1.TabIndex = 6;
            this.label1.Text = "Start typing CIK or the company name or select it from the list below";
            // 
            // ListOfCompaniesLB
            // 
            this.ListOfCompaniesLB.FormattingEnabled = true;
            this.ListOfCompaniesLB.Location = new System.Drawing.Point(6, 63);
            this.ListOfCompaniesLB.Name = "ListOfCompaniesLB";
            this.ListOfCompaniesLB.Size = new System.Drawing.Size(346, 160);
            this.ListOfCompaniesLB.TabIndex = 2;
            // 
            // CompanyNameTB
            // 
            this.CompanyNameTB.Location = new System.Drawing.Point(6, 39);
            this.CompanyNameTB.Name = "CompanyNameTB";
            this.CompanyNameTB.Size = new System.Drawing.Size(346, 20);
            this.CompanyNameTB.TabIndex = 0;
            this.CompanyNameTB.KeyUp += new System.Windows.Forms.KeyEventHandler(this.CompanyNameTB_KeyUp);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.LoadTags);
            this.groupBox1.Controls.Add(this.SelectTagsRefedit);
            this.groupBox1.Controls.Add(this.SelectedTags);
            this.groupBox1.Controls.Add(this.MoveTagToLeft);
            this.groupBox1.Controls.Add(this.MoveTagToRight);
            this.groupBox1.Controls.Add(this.TagsTree);
            this.groupBox1.Location = new System.Drawing.Point(12, 245);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(759, 384);
            this.groupBox1.TabIndex = 12;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Select Financial Facts";
            // 
            // LoadTags
            // 
            this.LoadTags.Location = new System.Drawing.Point(300, 351);
            this.LoadTags.Name = "LoadTags";
            this.LoadTags.Size = new System.Drawing.Size(51, 23);
            this.LoadTags.TabIndex = 20;
            this.LoadTags.Text = "Load";
            this.LoadTags.UseVisualStyleBackColor = true;
            this.LoadTags.Click += new System.EventHandler(this.LoadTags_Click);
            // 
            // SelectTagsRefedit
            // 
            this.SelectTagsRefedit._Excel = null;
            this.SelectTagsRefedit.AllowCollapsedFormResize = false;
            this.SelectTagsRefedit.Location = new System.Drawing.Point(5, 353);
            this.SelectTagsRefedit.Name = "SelectTagsRefedit";
            this.SelectTagsRefedit.Size = new System.Drawing.Size(289, 20);
            this.SelectTagsRefedit.TabIndex = 19;
            // 
            // SelectedTags
            // 
            this.SelectedTags.FormattingEnabled = true;
            this.SelectedTags.Location = new System.Drawing.Point(416, 18);
            this.SelectedTags.Name = "SelectedTags";
            this.SelectedTags.Size = new System.Drawing.Size(330, 329);
            this.SelectedTags.TabIndex = 18;
            // 
            // MoveTagToLeft
            // 
            this.MoveTagToLeft.Location = new System.Drawing.Point(357, 190);
            this.MoveTagToLeft.Name = "MoveTagToLeft";
            this.MoveTagToLeft.Size = new System.Drawing.Size(53, 23);
            this.MoveTagToLeft.TabIndex = 16;
            this.MoveTagToLeft.Text = "<<";
            this.MoveTagToLeft.UseVisualStyleBackColor = true;
            this.MoveTagToLeft.Click += new System.EventHandler(this.MoveTagToLeft_Click);
            // 
            // MoveTagToRight
            // 
            this.MoveTagToRight.Location = new System.Drawing.Point(357, 161);
            this.MoveTagToRight.Name = "MoveTagToRight";
            this.MoveTagToRight.Size = new System.Drawing.Size(53, 23);
            this.MoveTagToRight.TabIndex = 17;
            this.MoveTagToRight.Text = ">>";
            this.MoveTagToRight.UseVisualStyleBackColor = true;
            this.MoveTagToRight.Click += new System.EventHandler(this.MoveTagToRight_Click);
            // 
            // TagsTree
            // 
            this.TagsTree.CheckBoxes = true;
            this.TagsTree.FullRowSelect = true;
            this.TagsTree.Location = new System.Drawing.Point(5, 19);
            this.TagsTree.Name = "TagsTree";
            this.TagsTree.Size = new System.Drawing.Size(346, 326);
            this.TagsTree.TabIndex = 15;
            this.TagsTree.AfterCheck += new System.Windows.Forms.TreeViewEventHandler(this.TagsTree_AfterCheck_1);
            // 
            // Sc2_Historical
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.CancelButtonB;
            this.ClientSize = new System.Drawing.Size(785, 674);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.groupBox4);
            this.Controls.Add(this.CancelButtonB);
            this.Controls.Add(this.FinishB);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "Sc2_Historical";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Load historical data for several reporting periods";
            this.Shown += new System.EventHandler(this.Sc2_Historical_Shown);
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button FinishB;
        private System.Windows.Forms.Button CancelButtonB;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ListBox ListOfCompaniesLB;
        private System.Windows.Forms.TextBox CompanyNameTB;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Button LoadTags;
        private VS.NET_RefeditControl.refedit SelectTagsRefedit;
        private System.Windows.Forms.ListBox SelectedTags;
        private System.Windows.Forms.Button MoveTagToLeft;
        private System.Windows.Forms.Button MoveTagToRight;
        private System.Windows.Forms.TreeView TagsTree;
        private System.Windows.Forms.Button LoadSelectedCompanies;
        private VS.NET_RefeditControl.refedit SelectCompaniesRefedit;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Button MoveCompanyLeft;
        private System.Windows.Forms.Button MoveCompanyRight;
        private System.Windows.Forms.ListBox SelectedCompaniesLB;
    }
}