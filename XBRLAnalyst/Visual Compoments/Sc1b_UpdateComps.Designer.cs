﻿namespace XBRLAnalyst
{
    partial class Sc1b_UpdateComps
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.QuarterCB = new System.Windows.Forms.ComboBox();
            this.QuarterYearCB = new System.Windows.Forms.ComboBox();
            this.label13 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.SelectCompsTableRefedit = new VS.NET_RefeditControl.refedit();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.btnOK = new System.Windows.Forms.Button();
            this.btnCancel = new System.Windows.Forms.Button();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.QuarterCB);
            this.groupBox1.Controls.Add(this.QuarterYearCB);
            this.groupBox1.Controls.Add(this.label13);
            this.groupBox1.Controls.Add(this.label14);
            this.groupBox1.Location = new System.Drawing.Point(15, 77);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(238, 97);
            this.groupBox1.TabIndex = 8;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Select Comparison Period:";
            // 
            // QuarterCB
            // 
            this.QuarterCB.DisplayMember = "3";
            this.QuarterCB.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.QuarterCB.FormattingEnabled = true;
            this.QuarterCB.Items.AddRange(new object[] {
            "Annual",
            "1Q",
            "2Q",
            "3Q",
            "4Q"});
            this.QuarterCB.Location = new System.Drawing.Point(131, 55);
            this.QuarterCB.Name = "QuarterCB";
            this.QuarterCB.Size = new System.Drawing.Size(93, 21);
            this.QuarterCB.TabIndex = 4;
            // 
            // QuarterYearCB
            // 
            this.QuarterYearCB.DisplayMember = "3";
            this.QuarterYearCB.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.QuarterYearCB.FormattingEnabled = true;
            this.QuarterYearCB.Items.AddRange(new object[] {
            "2009",
            "2010",
            "2011",
            "2012",
            "2013"});
            this.QuarterYearCB.Location = new System.Drawing.Point(131, 24);
            this.QuarterYearCB.Name = "QuarterYearCB";
            this.QuarterYearCB.Size = new System.Drawing.Size(93, 21);
            this.QuarterYearCB.TabIndex = 3;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(6, 27);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(62, 13);
            this.label13.TabIndex = 1;
            this.label13.Text = "Fiscal Year:";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(6, 58);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(70, 13);
            this.label14.TabIndex = 1;
            this.label14.Text = "Fiscal Period:";
            // 
            // SelectCompsTableRefedit
            // 
            this.SelectCompsTableRefedit._Excel = null;
            this.SelectCompsTableRefedit.AllowCollapsedFormResize = false;
            this.SelectCompsTableRefedit.AllowTextEntry = false;
            this.SelectCompsTableRefedit.AutoSize = true;
            this.SelectCompsTableRefedit.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.SelectCompsTableRefedit.Location = new System.Drawing.Point(5, 26);
            this.SelectCompsTableRefedit.Name = "SelectCompsTableRefedit";
            this.SelectCompsTableRefedit.Size = new System.Drawing.Size(221, 20);
            this.SelectCompsTableRefedit.TabIndex = 14;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.SelectCompsTableRefedit);
            this.groupBox2.Location = new System.Drawing.Point(13, 13);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(240, 58);
            this.groupBox2.TabIndex = 15;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Select Range of Cells containing Comps Table";
            // 
            // btnOK
            // 
            this.btnOK.Location = new System.Drawing.Point(146, 180);
            this.btnOK.Name = "btnOK";
            this.btnOK.Size = new System.Drawing.Size(75, 23);
            this.btnOK.TabIndex = 16;
            this.btnOK.Text = "OK";
            this.btnOK.UseVisualStyleBackColor = true;
            this.btnOK.Click += new System.EventHandler(this.btnOK_Click);
            // 
            // btnCancel
            // 
            this.btnCancel.Location = new System.Drawing.Point(44, 180);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(75, 23);
            this.btnCancel.TabIndex = 16;
            this.btnCancel.Text = "Cancel";
            this.btnCancel.UseVisualStyleBackColor = true;
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // Sc1b_UpdateComps
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(264, 214);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.btnOK);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Name = "Sc1b_UpdateComps";
            this.Text = "Sc1b_UpdateComps";
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.ComboBox QuarterCB;
        private System.Windows.Forms.ComboBox QuarterYearCB;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label14;
        private VS.NET_RefeditControl.refedit SelectCompsTableRefedit;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Button btnOK;
        private System.Windows.Forms.Button btnCancel;
    }
}