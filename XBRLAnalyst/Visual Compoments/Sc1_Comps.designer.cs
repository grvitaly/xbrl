﻿namespace XBRLAnalyst
{
    partial class Sc1_Comps
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.MainTab = new System.Windows.Forms.TabControl();
            this.tabSelectCompanyPeriod = new System.Windows.Forms.TabPage();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.QuarterCB = new System.Windows.Forms.ComboBox();
            this.QuarterYearCB = new System.Windows.Forms.ComboBox();
            this.label13 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.label2 = new System.Windows.Forms.Label();
            this.ListOfIndustriesCB = new System.Windows.Forms.ListBox();
            this.IndustryNameTB = new System.Windows.Forms.TextBox();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.LoadSelectedCompanies = new System.Windows.Forms.Button();
            this.SelectCompaniesRefedit = new VS.NET_RefeditControl.refedit();
            this.label4 = new System.Windows.Forms.Label();
            this.MoveCompanyLeft = new System.Windows.Forms.Button();
            this.MoveCompanyRight = new System.Windows.Forms.Button();
            this.SelectedCompaniesLB = new System.Windows.Forms.ListBox();
            this.SelectNone = new System.Windows.Forms.LinkLabel();
            this.SelectAll = new System.Windows.Forms.LinkLabel();
            this.label1 = new System.Windows.Forms.Label();
            this.ListOfCompaniesLB = new System.Windows.Forms.ListBox();
            this.CompanyNameTB = new System.Windows.Forms.TextBox();
            this.tabSelectFinFacts = new System.Windows.Forms.TabPage();
            this.LoadTags = new System.Windows.Forms.Button();
            this.SelectTagsRefedit = new VS.NET_RefeditControl.refedit();
            this.SelectedTags = new System.Windows.Forms.ListBox();
            this.MoveTagToLeft = new System.Windows.Forms.Button();
            this.MoveTagToRight = new System.Windows.Forms.Button();
            this.TagsTree = new System.Windows.Forms.TreeView();
            this.label17 = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.CancelButtonB = new System.Windows.Forms.Button();
            this.FinishB = new System.Windows.Forms.Button();
            this.NextButton = new System.Windows.Forms.Button();
            this.MainTab.SuspendLayout();
            this.tabSelectCompanyPeriod.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox4.SuspendLayout();
            this.tabSelectFinFacts.SuspendLayout();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // MainTab
            // 
            this.MainTab.Controls.Add(this.tabSelectCompanyPeriod);
            this.MainTab.Controls.Add(this.tabSelectFinFacts);
            this.MainTab.Dock = System.Windows.Forms.DockStyle.Fill;
            this.MainTab.Location = new System.Drawing.Point(0, 0);
            this.MainTab.Name = "MainTab";
            this.MainTab.SelectedIndex = 0;
            this.MainTab.Size = new System.Drawing.Size(785, 472);
            this.MainTab.TabIndex = 0;
            this.MainTab.Selected += new System.Windows.Forms.TabControlEventHandler(this.MainTab_Selected);
            // 
            // tabSelectCompanyPeriod
            // 
            this.tabSelectCompanyPeriod.Controls.Add(this.groupBox1);
            this.tabSelectCompanyPeriod.Controls.Add(this.groupBox2);
            this.tabSelectCompanyPeriod.Controls.Add(this.groupBox4);
            this.tabSelectCompanyPeriod.Location = new System.Drawing.Point(4, 22);
            this.tabSelectCompanyPeriod.Name = "tabSelectCompanyPeriod";
            this.tabSelectCompanyPeriod.Padding = new System.Windows.Forms.Padding(3);
            this.tabSelectCompanyPeriod.Size = new System.Drawing.Size(777, 446);
            this.tabSelectCompanyPeriod.TabIndex = 0;
            this.tabSelectCompanyPeriod.Text = "Seclect Companies and Period";
            this.tabSelectCompanyPeriod.UseVisualStyleBackColor = true;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.QuarterCB);
            this.groupBox1.Controls.Add(this.QuarterYearCB);
            this.groupBox1.Controls.Add(this.label13);
            this.groupBox1.Controls.Add(this.label14);
            this.groupBox1.Location = new System.Drawing.Point(547, 9);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(222, 151);
            this.groupBox1.TabIndex = 7;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Select Comparison Period:";
            // 
            // QuarterCB
            // 
            this.QuarterCB.DisplayMember = "3";
            this.QuarterCB.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.QuarterCB.FormattingEnabled = true;
            this.QuarterCB.Items.AddRange(new object[] {
            "Annual",
            "1Q",
            "2Q",
            "3Q",
            "4Q"});
            this.QuarterCB.Location = new System.Drawing.Point(115, 86);
            this.QuarterCB.Name = "QuarterCB";
            this.QuarterCB.Size = new System.Drawing.Size(93, 21);
            this.QuarterCB.TabIndex = 4;
            // 
            // QuarterYearCB
            // 
            this.QuarterYearCB.DisplayMember = "3";
            this.QuarterYearCB.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.QuarterYearCB.FormattingEnabled = true;
            this.QuarterYearCB.Items.AddRange(new object[] {
            "2009",
            "2010",
            "2011",
            "2012",
            "2013"});
            this.QuarterYearCB.Location = new System.Drawing.Point(115, 47);
            this.QuarterYearCB.Name = "QuarterYearCB";
            this.QuarterYearCB.Size = new System.Drawing.Size(93, 21);
            this.QuarterYearCB.TabIndex = 3;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(19, 50);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(62, 13);
            this.label13.TabIndex = 1;
            this.label13.Text = "Fiscal Year:";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(15, 89);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(70, 13);
            this.label14.TabIndex = 1;
            this.label14.Text = "Fiscal Period:";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.label2);
            this.groupBox2.Controls.Add(this.ListOfIndustriesCB);
            this.groupBox2.Controls.Add(this.IndustryNameTB);
            this.groupBox2.Location = new System.Drawing.Point(9, 9);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(532, 151);
            this.groupBox2.TabIndex = 8;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Select Industry using SIC or Industry Name:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.ForeColor = System.Drawing.Color.Green;
            this.label2.Location = new System.Drawing.Point(7, 16);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(296, 13);
            this.label2.TabIndex = 7;
            this.label2.Text = "Start typing SIC or industry code or select it from the list below";
            // 
            // ListOfIndustriesCB
            // 
            this.ListOfIndustriesCB.FormattingEnabled = true;
            this.ListOfIndustriesCB.HorizontalScrollbar = true;
            this.ListOfIndustriesCB.Location = new System.Drawing.Point(10, 58);
            this.ListOfIndustriesCB.Name = "ListOfIndustriesCB";
            this.ListOfIndustriesCB.Size = new System.Drawing.Size(513, 82);
            this.ListOfIndustriesCB.TabIndex = 2;
            this.ListOfIndustriesCB.SelectedValueChanged += new System.EventHandler(this.ListOfIndustriesCB_SelectedValueChanged);
            // 
            // IndustryNameTB
            // 
            this.IndustryNameTB.Location = new System.Drawing.Point(10, 34);
            this.IndustryNameTB.Name = "IndustryNameTB";
            this.IndustryNameTB.Size = new System.Drawing.Size(513, 20);
            this.IndustryNameTB.TabIndex = 0;
            this.IndustryNameTB.KeyUp += new System.Windows.Forms.KeyEventHandler(this.IndustryNameTB_KeyUp);
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.LoadSelectedCompanies);
            this.groupBox4.Controls.Add(this.SelectCompaniesRefedit);
            this.groupBox4.Controls.Add(this.label4);
            this.groupBox4.Controls.Add(this.MoveCompanyLeft);
            this.groupBox4.Controls.Add(this.MoveCompanyRight);
            this.groupBox4.Controls.Add(this.SelectedCompaniesLB);
            this.groupBox4.Controls.Add(this.SelectNone);
            this.groupBox4.Controls.Add(this.SelectAll);
            this.groupBox4.Controls.Add(this.label1);
            this.groupBox4.Controls.Add(this.ListOfCompaniesLB);
            this.groupBox4.Controls.Add(this.CompanyNameTB);
            this.groupBox4.Location = new System.Drawing.Point(9, 166);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(760, 240);
            this.groupBox4.TabIndex = 9;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "Select Companies using CIK or Company Name:";
            // 
            // LoadSelectedCompanies
            // 
            this.LoadSelectedCompanies.Location = new System.Drawing.Point(671, 39);
            this.LoadSelectedCompanies.Name = "LoadSelectedCompanies";
            this.LoadSelectedCompanies.Size = new System.Drawing.Size(75, 20);
            this.LoadSelectedCompanies.TabIndex = 13;
            this.LoadSelectedCompanies.Text = "Load";
            this.LoadSelectedCompanies.UseVisualStyleBackColor = true;
            this.LoadSelectedCompanies.Click += new System.EventHandler(this.LoadSelectedCompanies_Click);
            // 
            // SelectCompaniesRefedit
            // 
            this.SelectCompaniesRefedit._Excel = null;
            this.SelectCompaniesRefedit.AllowCollapsedFormResize = false;
            this.SelectCompaniesRefedit.AllowTextEntry = false;
            this.SelectCompaniesRefedit.AutoSize = true;
            this.SelectCompaniesRefedit.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.SelectCompaniesRefedit.Location = new System.Drawing.Point(400, 39);
            this.SelectCompaniesRefedit.Name = "SelectCompaniesRefedit";
            this.SelectCompaniesRefedit.Size = new System.Drawing.Size(265, 20);
            this.SelectCompaniesRefedit.TabIndex = 12;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.ForeColor = System.Drawing.Color.Green;
            this.label4.Location = new System.Drawing.Point(397, 20);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(186, 13);
            this.label4.TabIndex = 11;
            this.label4.Text = "Select Range of Cells containing CIKs";
            // 
            // MoveCompanyLeft
            // 
            this.MoveCompanyLeft.Location = new System.Drawing.Point(358, 135);
            this.MoveCompanyLeft.Name = "MoveCompanyLeft";
            this.MoveCompanyLeft.Size = new System.Drawing.Size(36, 23);
            this.MoveCompanyLeft.TabIndex = 9;
            this.MoveCompanyLeft.Text = "<<";
            this.MoveCompanyLeft.UseVisualStyleBackColor = true;
            this.MoveCompanyLeft.Click += new System.EventHandler(this.MoveCompanyLeft_Click);
            // 
            // MoveCompanyRight
            // 
            this.MoveCompanyRight.Location = new System.Drawing.Point(358, 106);
            this.MoveCompanyRight.Name = "MoveCompanyRight";
            this.MoveCompanyRight.Size = new System.Drawing.Size(36, 23);
            this.MoveCompanyRight.TabIndex = 9;
            this.MoveCompanyRight.Text = ">>";
            this.MoveCompanyRight.UseVisualStyleBackColor = true;
            this.MoveCompanyRight.Click += new System.EventHandler(this.MoveCompanyRight_Click);
            // 
            // SelectedCompaniesLB
            // 
            this.SelectedCompaniesLB.FormattingEnabled = true;
            this.SelectedCompaniesLB.Location = new System.Drawing.Point(400, 63);
            this.SelectedCompaniesLB.Name = "SelectedCompaniesLB";
            this.SelectedCompaniesLB.Size = new System.Drawing.Size(346, 147);
            this.SelectedCompaniesLB.TabIndex = 8;
            // 
            // SelectNone
            // 
            this.SelectNone.AutoSize = true;
            this.SelectNone.Location = new System.Drawing.Point(85, 217);
            this.SelectNone.Name = "SelectNone";
            this.SelectNone.Size = new System.Drawing.Size(63, 13);
            this.SelectNone.TabIndex = 7;
            this.SelectNone.TabStop = true;
            this.SelectNone.Text = "Deselect All";
            this.SelectNone.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.SelectNone_LinkClicked);
            // 
            // SelectAll
            // 
            this.SelectAll.AutoSize = true;
            this.SelectAll.Location = new System.Drawing.Point(10, 217);
            this.SelectAll.Name = "SelectAll";
            this.SelectAll.Size = new System.Drawing.Size(51, 13);
            this.SelectAll.TabIndex = 7;
            this.SelectAll.TabStop = true;
            this.SelectAll.Text = "Select All";
            this.SelectAll.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.SelectAll_LinkClicked);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.ForeColor = System.Drawing.Color.Green;
            this.label1.Location = new System.Drawing.Point(7, 20);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(323, 13);
            this.label1.TabIndex = 6;
            this.label1.Text = "Start typing CIK or the company name or select it from the list below";
            // 
            // ListOfCompaniesLB
            // 
            this.ListOfCompaniesLB.FormattingEnabled = true;
            this.ListOfCompaniesLB.Location = new System.Drawing.Point(6, 63);
            this.ListOfCompaniesLB.Name = "ListOfCompaniesLB";
            this.ListOfCompaniesLB.SelectionMode = System.Windows.Forms.SelectionMode.MultiExtended;
            this.ListOfCompaniesLB.Size = new System.Drawing.Size(346, 147);
            this.ListOfCompaniesLB.TabIndex = 2;
            // 
            // CompanyNameTB
            // 
            this.CompanyNameTB.Location = new System.Drawing.Point(6, 39);
            this.CompanyNameTB.Name = "CompanyNameTB";
            this.CompanyNameTB.Size = new System.Drawing.Size(346, 20);
            this.CompanyNameTB.TabIndex = 0;
            this.CompanyNameTB.KeyUp += new System.Windows.Forms.KeyEventHandler(this.CompanyNameTB_KeyUp);
            // 
            // tabSelectFinFacts
            // 
            this.tabSelectFinFacts.Controls.Add(this.LoadTags);
            this.tabSelectFinFacts.Controls.Add(this.SelectTagsRefedit);
            this.tabSelectFinFacts.Controls.Add(this.SelectedTags);
            this.tabSelectFinFacts.Controls.Add(this.MoveTagToLeft);
            this.tabSelectFinFacts.Controls.Add(this.MoveTagToRight);
            this.tabSelectFinFacts.Controls.Add(this.TagsTree);
            this.tabSelectFinFacts.Controls.Add(this.label17);
            this.tabSelectFinFacts.Location = new System.Drawing.Point(4, 22);
            this.tabSelectFinFacts.Name = "tabSelectFinFacts";
            this.tabSelectFinFacts.Padding = new System.Windows.Forms.Padding(3);
            this.tabSelectFinFacts.Size = new System.Drawing.Size(777, 446);
            this.tabSelectFinFacts.TabIndex = 1;
            this.tabSelectFinFacts.Text = "Select Financial Facts";
            this.tabSelectFinFacts.UseVisualStyleBackColor = true;
            // 
            // LoadTags
            // 
            this.LoadTags.Location = new System.Drawing.Point(301, 386);
            this.LoadTags.Name = "LoadTags";
            this.LoadTags.Size = new System.Drawing.Size(51, 23);
            this.LoadTags.TabIndex = 13;
            this.LoadTags.Text = "Load";
            this.LoadTags.UseVisualStyleBackColor = true;
            this.LoadTags.Click += new System.EventHandler(this.LoadTags_Click);
            // 
            // SelectTagsRefedit
            // 
            this.SelectTagsRefedit._Excel = null;
            this.SelectTagsRefedit.AllowCollapsedFormResize = false;
            this.SelectTagsRefedit.Location = new System.Drawing.Point(6, 388);
            this.SelectTagsRefedit.Name = "SelectTagsRefedit";
            this.SelectTagsRefedit.Size = new System.Drawing.Size(289, 20);
            this.SelectTagsRefedit.TabIndex = 12;
            // 
            // SelectedTags
            // 
            this.SelectedTags.FormattingEnabled = true;
            this.SelectedTags.Location = new System.Drawing.Point(417, 29);
            this.SelectedTags.Name = "SelectedTags";
            this.SelectedTags.Size = new System.Drawing.Size(285, 381);
            this.SelectedTags.TabIndex = 7;
            // 
            // MoveTagToLeft
            // 
            this.MoveTagToLeft.Location = new System.Drawing.Point(358, 271);
            this.MoveTagToLeft.Name = "MoveTagToLeft";
            this.MoveTagToLeft.Size = new System.Drawing.Size(53, 23);
            this.MoveTagToLeft.TabIndex = 6;
            this.MoveTagToLeft.Text = "<<";
            this.MoveTagToLeft.UseVisualStyleBackColor = true;
            this.MoveTagToLeft.Click += new System.EventHandler(this.MoveTagToLeft_Click);
            // 
            // MoveTagToRight
            // 
            this.MoveTagToRight.Location = new System.Drawing.Point(358, 242);
            this.MoveTagToRight.Name = "MoveTagToRight";
            this.MoveTagToRight.Size = new System.Drawing.Size(53, 23);
            this.MoveTagToRight.TabIndex = 6;
            this.MoveTagToRight.Text = ">>";
            this.MoveTagToRight.UseVisualStyleBackColor = true;
            this.MoveTagToRight.Click += new System.EventHandler(this.MoveTagToRight_Click);
            // 
            // TagsTree
            // 
            this.TagsTree.CheckBoxes = true;
            this.TagsTree.FullRowSelect = true;
            this.TagsTree.Location = new System.Drawing.Point(6, 29);
            this.TagsTree.Name = "TagsTree";
            this.TagsTree.Size = new System.Drawing.Size(346, 351);
            this.TagsTree.TabIndex = 5;
            this.TagsTree.AfterCheck += new System.Windows.Forms.TreeViewEventHandler(this.TagsTree_AfterCheck);
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(8, 13);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(177, 13);
            this.label17.TabIndex = 3;
            this.label17.Text = "Choose Facts you want to compare:";
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.CancelButtonB);
            this.panel1.Controls.Add(this.FinishB);
            this.panel1.Controls.Add(this.NextButton);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel1.Location = new System.Drawing.Point(0, 436);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(785, 36);
            this.panel1.TabIndex = 1;
            // 
            // CancelButtonB
            // 
            this.CancelButtonB.Location = new System.Drawing.Point(262, 6);
            this.CancelButtonB.Name = "CancelButtonB";
            this.CancelButtonB.Size = new System.Drawing.Size(75, 23);
            this.CancelButtonB.TabIndex = 2;
            this.CancelButtonB.Text = "&Cancel";
            this.CancelButtonB.UseVisualStyleBackColor = true;
            this.CancelButtonB.Click += new System.EventHandler(this.CancelButtonB_Click);
            // 
            // FinishB
            // 
            this.FinishB.Location = new System.Drawing.Point(424, 6);
            this.FinishB.Name = "FinishB";
            this.FinishB.Size = new System.Drawing.Size(75, 23);
            this.FinishB.TabIndex = 1;
            this.FinishB.Text = "&Finish";
            this.FinishB.UseVisualStyleBackColor = true;
            this.FinishB.Click += new System.EventHandler(this.FinishB_Click);
            // 
            // NextButton
            // 
            this.NextButton.Location = new System.Drawing.Point(343, 6);
            this.NextButton.Name = "NextButton";
            this.NextButton.Size = new System.Drawing.Size(75, 23);
            this.NextButton.TabIndex = 0;
            this.NextButton.Text = "&Next >>";
            this.NextButton.UseVisualStyleBackColor = true;
            this.NextButton.Click += new System.EventHandler(this.NextButton_Click);
            // 
            // Sc1_Comps
            // 
            this.AcceptButton = this.NextButton;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(785, 472);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.MainTab);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "Sc1_Comps";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Get data for multiple companies in single reporting period";
            this.Shown += new System.EventHandler(this.Sc1_Comps_Shown);
            this.MainTab.ResumeLayout(false);
            this.tabSelectCompanyPeriod.ResumeLayout(false);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            this.tabSelectFinFacts.ResumeLayout(false);
            this.tabSelectFinFacts.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TabControl MainTab;
        private System.Windows.Forms.TabPage tabSelectFinFacts;
        private System.Windows.Forms.TabPage tabSelectCompanyPeriod;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.ComboBox QuarterCB;
        private System.Windows.Forms.ComboBox QuarterYearCB;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.ListBox ListOfIndustriesCB;
        private System.Windows.Forms.TextBox IndustryNameTB;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.ListBox ListOfCompaniesLB;
        private System.Windows.Forms.TextBox CompanyNameTB;
        private System.Windows.Forms.TreeView TagsTree;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Button FinishB;
        private System.Windows.Forms.Button NextButton;
        private System.Windows.Forms.Button CancelButtonB;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.LinkLabel SelectNone;
        private System.Windows.Forms.LinkLabel SelectAll;
        private System.Windows.Forms.Button MoveCompanyLeft;
        private System.Windows.Forms.Button MoveCompanyRight;
        private System.Windows.Forms.ListBox SelectedCompaniesLB;
        private System.Windows.Forms.ListBox SelectedTags;
        private System.Windows.Forms.Button MoveTagToLeft;
        private System.Windows.Forms.Button MoveTagToRight;
        private VS.NET_RefeditControl.refedit SelectCompaniesRefedit;
        private System.Windows.Forms.Label label4;
        private VS.NET_RefeditControl.refedit SelectTagsRefedit;
        private System.Windows.Forms.Button LoadSelectedCompanies;
        private System.Windows.Forms.Button LoadTags;
    }
}